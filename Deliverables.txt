Pushing Instructions

Session 1

Go to Gitlab:
	-in your zuitt-projects folder and access b152 folder.
	-inside your b152 folder create a new repo called capstone2-<lastName>
	-untick the readme option
	-copy the git url from the clone button of your capstone2-<lastName> repo.

Go to Gitbash:
	-create a new file in capstone2-<lastName> folder
		-title: .gitignore
		-content: /node_modules
	-go to your b152/capstone2-<lastName> folder.
	-initialize capstone2-<lastName> folder as a local repo: git init
	-connect your local repo to our online repo: git remote add origin <gitURLOfOnlineRepo>
	-add your updates to be committed: git add .
	-commit your changes to be pushed: git commit -m "includes Session 1 updates for csp2"
	-push your updates to your online repo: git push origin master

Go to boodle:
	
	-Link your capstone2-<lastName> repo to boodle:
	WDC028-37 | Capstone 2 Development - Ecommerce API

Session 2

Pushing Instructions


Go to Gitbash:

	-add your updates to be committed: git add .
	-commit your changes to be pushed: git commit -m "includes Session 2 updates for csp2"
	-push your updates to your online repo: git push origin master

Go to boodle:
	
	-Link your capstone2-<lastName> repo to boodle:
	WDC028-38 | Capstone 2 Development - Ecommerce API

Session 3

Pushing Instructions


Go to Gitbash:

	-add your updates to be committed: git add .
	-commit your changes to be pushed: git commit -m "includes Session 3 updates for csp2"
	-push your updates to your online repo: git push origin master

Go to boodle:
	
	-Link your capstone2-<lastName> repo to boodle:
	WDC028-39 | Capstone 2 Development - Ecommerce API


Session 4

Pushing Instructions


Go to Gitbash:

	-add your updates to be committed: git add .
	-commit your changes to be pushed: git commit -m "includes Session 4 updates for csp2"
	-push your updates to your online repo: git push origin master

Go to boodle:
	
	-Link your capstone2-<lastName> repo to boodle:
	WDC028-40 | Capstone 2 Development - Ecommerce API


Session 5

Pushing Instructions


Go to Gitbash:

	-add your updates to be committed: git add .
	-commit your changes to be pushed: git commit -m "includes Session 5 updates for csp2"
	-push your updates to your online repo: git push origin master

Go to boodle:
	
	-Link your capstone2-<lastName> repo to boodle:
	WDC028-41 | Capstone 2 Development - Ecommerce API