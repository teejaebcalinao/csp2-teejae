Project Name: TeeJae Store E-Commerce API

Admin Credentials:

email: "admin@gmail.com"
password: "admin123"

Regular User Credentials:

email: "sampleUser@gmail.com"
password: sample123

Features:
User registration
User authentication
Set user as admin (Admin only)
Retrieve all active products
Retrieve single product
Create Product (Admin only)
Update Product information (Admin only)
Archive Product (Admin only)
Non-admin User checkout (Create Order)
Retrieve authenticated user’s orders
Retrieve all orders (Admin only)